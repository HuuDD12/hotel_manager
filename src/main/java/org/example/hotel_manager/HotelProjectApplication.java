package org.example.hotel_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@SpringBootApplication
public class HotelProjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(HotelProjectApplication.class, args);
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        String rawPassword = "1211";
//        String encodedPassword = encoder.encode(rawPassword);
//        System.out.println(encodedPassword);
    }
}